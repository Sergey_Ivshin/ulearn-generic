﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PCHMI_Generic
{
    public class ProcessorFor<TEngine, TEntity>
    {
        public Processor<TEngine, TEntity, TLogger> With<TLogger>()
        {
            return new Processor<TEngine, TEntity, TLogger>();
        }
    }
}
