﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PCHMI_Generic
{
    public class ProcessorCreator<TEngine>
    {
        public ProcessorFor<TEngine, TEntity> For<TEntity>()
        {
            return new ProcessorFor<TEngine, TEntity>();
        }
    }
}
