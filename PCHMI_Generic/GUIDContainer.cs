﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCHMI_Generic
{
    class GUIDContainer
    {
        Dictionary<Guid, object> dictionary;
        public GUIDContainer() {
            dictionary = new Dictionary<Guid, object>();
        }
        public T CreateObject<T>() where T : new()
        {
            var newobject = new T();
            dictionary.Add(Guid.NewGuid(), newobject);
            return newobject;
        } 
        public List<KeyValuePair<Guid,object>> ShowDictionaryForType<T>(){
            var pairs = dictionary.Where(x => x.Value is T).Select(x=>new KeyValuePair<Guid,object>(x.Key,x.Value)).ToList();
            return pairs;
        }
        public object getObject(Guid guid){
            object returnObject = null;
            returnObject = dictionary.Where(x => x.Key == guid).FirstOrDefault().Value;
            return returnObject;
        }

    }
}
