﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCHMI_Generic
{
    class Program
    {
        static void Main(string[] args)
        {
            var proc = Processor.CreateEngine<MyEngine>().For<MyEntity>().With<MyLogger>();
            GUIDContainer guidContainer = new GUIDContainer();
            MyEngine myEngine = guidContainer.CreateObject<MyEngine>();
            MyEntity myEntity = guidContainer.CreateObject<MyEntity>();
            MyLogger myLogger = guidContainer.CreateObject<MyLogger>();
            MyLogger myLogger1 = guidContainer.CreateObject<MyLogger>();
            var searchObjects = guidContainer.ShowDictionaryForType<MyLogger>();
            var obj1 = guidContainer.getObject(searchObjects.First().Key);
            var obj2 = guidContainer.getObject(Guid.NewGuid());
        }
    }
}
